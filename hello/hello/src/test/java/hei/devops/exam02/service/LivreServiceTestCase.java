package hei.devops.exam02.service;

import hei.devops.exam02.entity.Livre;
import hei.devops.exam02.exception.LivreAlreadyExistsException;
import hei.devops.exam02.exception.LivreNotFoundException;
import hei.devops.exam02.exception.ParamEmptyException;
import hei.devops.exam02.service.impl.LivreServiceImpl;
import org.junit.Test;

import static org.junit.Assert.*;

public class LivreServiceTestCase {
    LivreService livreServiceImpl;


    @Test
    public void getInstance() {
        livreServiceImpl = LivreServiceImpl.getInstance();
        assertNotNull(livreServiceImpl);
    }

    @Test
    public void Test() {
        livreServiceImpl = LivreServiceImpl.getInstance();
        Exception exception = assertThrows(LivreAlreadyExistsException.class, () -> {
            livreServiceImpl.add("a", "b", "c");
            livreServiceImpl.add("a", "b", "c");
            assertEquals(livreServiceImpl.find("c"), "c");
        });

        String expectedMessage = "Livre already";
        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(expectedMessage));

    }

    @Test
    public void Test2() {
        livreServiceImpl = LivreServiceImpl.getInstance();
        Exception exception = assertThrows(ParamEmptyException.class, () -> {
            livreServiceImpl.add(null, "b", "c");
        });

        String expectedMessage = "Param";
        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(expectedMessage));

    }

    @Test
    public void Test3() throws ParamEmptyException, LivreAlreadyExistsException {
        livreServiceImpl = LivreServiceImpl.getInstance();
        livreServiceImpl.add("x", "y", "z");
        Livre livre = new Livre("a", "b", "c");
        Exception exception = assertThrows(LivreNotFoundException.class, () -> {
            assertEquals(livreServiceImpl.find("c").getAuteur(), livre.getAuteur());
        });
        String expectedMessage = "Livre is not found";
        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    public void Test4() {
        livreServiceImpl = LivreServiceImpl.getInstance();
        Livre livre = new Livre("a", "b", "c");
        Exception exception = assertThrows(LivreNotFoundException.class, () -> {
            assertEquals(livreServiceImpl.find("c").getAuteur(), livre.getAuteur());
        });
        String expectedMessage = "Livre is not found";
        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(expectedMessage));
    }


}