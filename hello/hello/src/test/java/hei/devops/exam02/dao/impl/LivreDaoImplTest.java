package hei.devops.exam02.dao.impl;

import hei.devops.exam02.entity.Livre;
import org.junit.Test;

import static org.junit.Assert.*;

public class LivreDaoImplTest {

    LivreDaoImpl livreDaoImpl;

    @Test
    public void getInstanceTest() {
        livreDaoImpl = new LivreDaoImpl();
        assertNotNull(LivreDaoImpl.getInstance());
    }

    @Test
    public void Test() {
        livreDaoImpl = new LivreDaoImpl();
        assertNotNull(livreDaoImpl.read());
    }

    @Test
    public void Test1() {
        livreDaoImpl = new LivreDaoImpl();
        Livre livre = new Livre("a", "b", "c");
        livreDaoImpl.create(livre);
        livreDaoImpl.create(livre);
        livreDaoImpl.create(livre);
        assertEquals(livreDaoImpl.read().size(), 3);
    }

    @Test
    public void Test2() {
        livreDaoImpl = new LivreDaoImpl();
        Livre livre = new Livre("a", "b", "c");
        livreDaoImpl.create(livre);
        livreDaoImpl.create(livre);
        assertNotEquals(livreDaoImpl.read().size(), 1);
    }

    @Test
    public void Test3() {
        livreDaoImpl = new LivreDaoImpl();
        Livre livre = new Livre("a", "b", "c");
        Livre livre2 = new Livre("g", "x", "e");
        livreDaoImpl.create(livre);
        livreDaoImpl.create(livre2);
        assertEquals(livreDaoImpl.read().get(0).getAuteur(), "c");
    }

    @Test
    public void Test4() {
        livreDaoImpl = new LivreDaoImpl();
        Exception exception = assertThrows(IndexOutOfBoundsException.class, () -> {
            assertEquals(livreDaoImpl.read().get(0).getAuteur(), "c");
        });

        String expectedMessage = "Index";
        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(expectedMessage));

    }

}