package hei.devops.exam02.entity;


import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class LivreTest {
    Livre livre;


    @Test
    public void getIsbnTest() {
        livre = new Livre("a", "b", "c");
        assertEquals(livre.getIsbn(), "a");
    }

    @Test
    public void setIsbnTest() {
        livre = new Livre("a", "b", "c");
        livre.setIsbn("t");
        assertEquals(livre.getIsbn(), "t");
    }

    @Test
    public void getTitreTest() {
        livre = new Livre("a", "b", "c");

        assertEquals(livre.getTitre(), "b");
    }

    @Test
    public void setTitreTest() {
        livre = new Livre("a", "b", "c");
        livre.setTitre("t");
        assertEquals(livre.getTitre(), "t");
    }

    @Test
    public void getAuteurTest() {
        livre = new Livre("a", "b", "c");

        assertEquals(livre.getAuteur(), "c");
    }

    @Test
    public void setAuteurTest() {
        livre = new Livre("a", "b", "c");
        livre.setAuteur("t");
        assertEquals(livre.getAuteur(), "t");
    }
}