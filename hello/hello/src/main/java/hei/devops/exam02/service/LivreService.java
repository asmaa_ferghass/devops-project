package hei.devops.exam02.service;

import hei.devops.exam02.entity.Livre;
import hei.devops.exam02.exception.LivreAlreadyExistsException;
import hei.devops.exam02.exception.LivreNotFoundException;
import hei.devops.exam02.exception.ParamEmptyException;

public interface LivreService {

    public void add(String isbn, String titre, String auteur) throws LivreAlreadyExistsException, ParamEmptyException;

    public Livre find(String isbn) throws LivreNotFoundException;

}
