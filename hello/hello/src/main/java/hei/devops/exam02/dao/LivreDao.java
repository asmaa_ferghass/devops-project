package hei.devops.exam02.dao;

import hei.devops.exam02.entity.Livre;

import java.util.List;

public interface LivreDao {

    public List<Livre> read();

    public void create(Livre livre);

}
