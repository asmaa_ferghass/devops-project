package hei.devops.exam02.exception;

public class LivreAlreadyExistsException extends Exception {

    /**
     *
     */
    private static final long serialVersionUID = 5739198223481008652L;

    public LivreAlreadyExistsException(String isbn) {
        super("Livre already exists with isbn=" + isbn);
    }

}
