package hei.devops.exam02.dao.impl;

import hei.devops.exam02.dao.LivreDao;
import hei.devops.exam02.entity.Livre;

import java.util.ArrayList;
import java.util.List;

public class LivreDaoImpl implements LivreDao {

    private static LivreDao instance = null;
    private List<Livre> livres;

    public LivreDaoImpl() {
        livres = new ArrayList<Livre>();
    }

    public static LivreDao getInstance() {
        if (instance == null) {
            instance = new LivreDaoImpl();
        }
        return instance;
    }

    @Override
    public List<Livre> read() {
        return livres;
    }

    @Override
    public void create(Livre livre) {
        livres.add(livre);
    }

}
