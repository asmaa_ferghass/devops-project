package hei.devops.exam02;

import java.util.Scanner;

import hei.devops.exam02.entity.Livre;
import hei.devops.exam02.exception.LivreNotFoundException;
import hei.devops.exam02.service.LivreService;
import hei.devops.exam02.service.impl.LivreServiceImpl;

public class Application {

	private static LivreService livreService;
	
	public static void main(String[] args) throws InterruptedException {
		livreService = LivreServiceImpl.getInstance();
		showExample();

		while(true){
			
			String answer = getAnswer();
			String elements[] =answer.split("\\|");
			if("add".equals(elements[0])){
				try {
					livreService.add(elements[1], elements[2], elements[3]);
					System.out.println("livre added");
				} catch (Exception e) {
					System.err.println(e.getMessage());
				}


			}
			else if("find".equals(elements[0])){
				try {
					Livre livre = livreService.find(elements[1]);
					System.out.println("livre found : " + livre.getTitre() + "  ( " +livre.getAuteur()+" )");
				} catch (LivreNotFoundException e) {
					System.err.println(e.getMessage());
				}
			}
			else if("exit".equals(elements[0])) {
				return ;
			}
			else {
				System.err.println("Bad request !!!");
				System.err.println(answer + " is not correct !!!");
				Thread.sleep(1000);
				showExample();
			}
			System.out.println();
			Thread.sleep(1000);


		}

	}

	private static String getAnswer() {
		System.out.println("Write your request : ");
		return getScanner().nextLine();		
	}

	private static Scanner getScanner() {
		return new Scanner(System.in);
	}

	private static void showExample() {
		System.out.println(" Example : ");
		System.out.println("  - add|isbn|titre|auteur");
		System.out.println("  - find|isbn");
		System.out.println("  - exit");
	}


}