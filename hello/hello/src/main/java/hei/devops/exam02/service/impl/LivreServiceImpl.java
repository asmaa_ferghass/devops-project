package hei.devops.exam02.service.impl;

import hei.devops.exam02.dao.LivreDao;
import hei.devops.exam02.dao.impl.LivreDaoImpl;
import hei.devops.exam02.entity.Livre;
import hei.devops.exam02.exception.LivreAlreadyExistsException;
import hei.devops.exam02.exception.LivreNotFoundException;
import hei.devops.exam02.exception.ParamEmptyException;
import hei.devops.exam02.service.LivreService;

public class LivreServiceImpl implements LivreService {

    private static LivreService instance = null;
    private LivreDao livreDao;

    public LivreServiceImpl() {
        livreDao = LivreDaoImpl.getInstance();
    }

    public static LivreService getInstance() {
        if (instance == null) {
            instance = new LivreServiceImpl();
        }
        return instance;
    }

    @Override
    public void add(String isbn, String titre, String auteur)
            throws LivreAlreadyExistsException, ParamEmptyException {

        checkParam("isbn", isbn);
        checkParam("titre", titre);
        checkParam("auteur", auteur);

        if (getLivre(isbn) == null) {
            livreDao.create(new Livre(isbn, titre, auteur));
        } else {
            throw new LivreAlreadyExistsException(isbn);
        }
    }

    @Override
    public Livre find(String isbn) throws LivreNotFoundException {

        Livre livre = getLivre(isbn);

        if (livre != null) {
            return livre;
        }

        throw new LivreNotFoundException(isbn);
    }

    private Livre getLivre(String isbn) {
        for (Livre livre : livreDao.read()) {
            if (livre.getIsbn().equals(isbn)) {
                return livre;
            }
        }
        return null;
    }

    private void checkParam(String param, String value) throws ParamEmptyException {
        if (value != null) {
            if (!value.isBlank()) {
                return;
            }
        }

        throw new ParamEmptyException(param);

    }
}
