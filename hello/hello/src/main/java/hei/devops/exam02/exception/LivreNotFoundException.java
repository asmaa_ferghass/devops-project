package hei.devops.exam02.exception;

public class LivreNotFoundException extends Exception {

    /**
     *
     */
    private static final long serialVersionUID = 2342556426476844916L;

    public LivreNotFoundException(String isbn) {
        super("Livre is not found with isbn=" + isbn);
    }

}
