package hei.devops.exam02.exception;

public class ParamEmptyException extends Exception {

    /**
     *
     */
    private static final long serialVersionUID = -3087729195323951762L;

    public ParamEmptyException(String paramName) {
        super("Param " + paramName + " is emtpy");
    }

}
