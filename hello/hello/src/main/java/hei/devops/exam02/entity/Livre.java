package hei.devops.exam02.entity;

public class Livre {

    private String isbn;
    private String titre;
    private String auteur;

    public Livre(String isbn, String titre, String auteur) {
        super();
        this.isbn = isbn;
        this.titre = titre;
        this.auteur = auteur;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getAuteur() {
        return auteur;
    }

    public void setAuteur(String auteur) {
        this.auteur = auteur;
    }

}
